package main

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/go-redis/redis"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getBeastNumber(beast string) string {

	redis_server := getEnv("REDIS_SERVER", "localhost")
	redis_port := getEnv("REDIS_PORT", "6379")
	redis_password := getEnv("REDIS_PASSWORD", "")
	redis_db := getEnv("REDIS_DB", "0")
	redis_db_name, err := strconv.Atoi(redis_db)

	client := redis.NewClient(&redis.Options{
		Addr:     redis_server + ":" + redis_port,
		Password: redis_password,
		DB:       redis_db_name,
	})

	val, err := client.Get(beast).Result()
	if err != nil {
		log.Println(err)
		return "0"
	}

	return val
}

func write(writer http.ResponseWriter, message string) {
	_, err := writer.Write([]byte(message))
	if err != nil {
		log.Fatal(err)
	}
}

func chickendHandler(writer http.ResponseWriter, request *http.Request) {
	beastNumber := getBeastNumber("chicken")
	write(writer, "Hello chickens! "+beastNumber)

}

func duckdHandler(writer http.ResponseWriter, request *http.Request) {
	beastNumber := getBeastNumber("duck")
	write(writer, "Hello ducks! "+beastNumber)
}

func gooseHandler(writer http.ResponseWriter, request *http.Request) {
	beastNumber := getBeastNumber("goose")
	write(writer, "Hello gooses! "+beastNumber)

}

func foxHandler(writer http.ResponseWriter, request *http.Request) {
	beastNumber := getBeastNumber("fox")
	write(writer, "Hello foxes! "+beastNumber)
}

func main() {

	http.HandleFunc("/chicks", chickendHandler)
	http.HandleFunc("/ducks", duckdHandler)
	http.HandleFunc("/gooses", gooseHandler)
	http.HandleFunc("/foxes", foxHandler)

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}
	httpAddress := os.Getenv("HTTP_ADDRESS")
	if httpPort == "" {
		httpPort = "0.0.0.0"
	}
	err := http.ListenAndServe(httpAddress+":"+httpPort, nil)
	log.Fatal(err)

}
